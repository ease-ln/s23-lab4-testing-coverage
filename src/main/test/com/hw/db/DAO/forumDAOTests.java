package com.hw.db.dao;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import static org.mockito.Mockito.*;

// Full branch coverage for function UserList
class forumDAO {
    JdbcTemplate mockJdbc;
    ForumDAO forum;
    UserDAO.UserMapper USER_MAPPER;

    @BeforeEach
    void init() {
        mockJdbc = mock(JdbcTemplate.class);
        forum = new ForumDAO(mockJdbc);
        USER_MAPPER = new UserDAO.UserMapper();
    }

    @Test
    @DisplayName("User gets list of threads test #1")
    void ThreadListTest1() {
        ForumDAO.UserList("slug",null, null, false);
        verify(mockJdbc).query(
            Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"),
            Mockito.any(Object[].class),
            Mockito.any(ThreadDAO.ThreadMapper.class));
    }
}