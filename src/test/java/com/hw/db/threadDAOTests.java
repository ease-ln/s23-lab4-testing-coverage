import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;

import com.hw.db.models.Post;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;

import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import static org.mockito.Mockito.*;
import java.sql.Timestamp;

// Full statement coverage for function treeSort 
class threadDAOTests {
    JdbcTemplate mockJdbc;
    ThreadDAO mockThread;

    @BeforeEach
    void init() {
        mockJdbc = mock(JdbcTemplate.class);
        mockThread = new ThreadDAO(mockJdbc);
    }

    @Test
    @DisplayName("Thread test #1")
    void TreeSortTest1() {
        ThreadDAO.treeSort(1, 2, 3, null);
        Mockito.verify(mockJdbc).query(
            Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"),
            Mockito.any(PostDAO.PostMapper.class),
            Mockito.eq(1),
            Mockito.eq(3),
            Mockito.eq(2)
            );
    }

    @Test
    @DisplayName("Thread test #2")
    void TreeSortTest2() {
        ThreadDAO.treeSort(1, 2, 3, true);
        Mockito.verify(mockJdbc).query(
            Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"),
            Mockito.any(PostDAO.PostMapper.class),
            Mockito.eq(1),
            Mockito.eq(3),
            Mockito.eq(2)
            );
    }
}
