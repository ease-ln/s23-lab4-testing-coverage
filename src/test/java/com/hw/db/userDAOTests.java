import com.hw.db.DAO.UserDAO;
import com.hw.db.DAO.ForumDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;

import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import static org.mockito.Mockito.*;


// Full MC/DC coverage for function Change
class userDAOTests {
    JdbcTemplate mockJdbc;
    User user;
    UserDAO.UserMapper USER_MAPPER;

    @BeforeEach
    void init() {
        mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);
    }

    @Test
    @DisplayName("Change user test #1")
    void ThreadListTest1() {
        User user = new User("nickname", "mail", "fullname", "about");
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("mail"),
                Mockito.eq("fullname"),
                Mockito.eq("about"),
                Mockito.eq(user.getNickname()));
    }
    
    @Test
    @DisplayName("Change user test #2")
    void ThreadListTest2() {
        User user = new User("nickname", "mail", "fullname", null);
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("mail"),
                Mockito.eq("fullname"),
                Mockito.eq(user.getNickname()));
    }

    @Test
    @DisplayName("Change user test #3")
    void ThreadListTest3() {
        User user = new User("nickname", "mail", null, "about");
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("mail"),
                Mockito.eq("about"),
                Mockito.eq(user.getNickname()));
    }

    @Test
    @DisplayName("Change user test #4")
    void ThreadListTest4() {
        User user = new User("nickname", "mail", null, null);
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("mail"),
                Mockito.eq(user.getNickname()));
    }

    @Test
    @DisplayName("Change user test #5")
    void ThreadListTest5() {
        User user = new User("nickname", null, "fullname", "about");
        UserDAO.Change(user);        
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("fullname"),
                Mockito.eq("about"),
                Mockito.eq(user.getNickname()));
    }

    @Test
    @DisplayName("Change user test #6")
    void ThreadListTest6() {
        User user = new User("nickname", null, "fullname", null);
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("fullname"),
                Mockito.eq(user.getNickname()));
    }

    @Test
    @DisplayName("Change user test #7")
    void ThreadListTest7() {
        User user = new User("nickname", null, null, "about");
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("about"),
                Mockito.eq(user.getNickname()));
    }

    @Test
    @DisplayName("Change user test #8")
    void UserChangeTest8() {
        User user = new User(null, null, null, null);
        UserDAO.Change(user);
        verifyNoInteractions(mockJdbc);
    }
}