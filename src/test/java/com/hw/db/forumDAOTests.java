import com.hw.db.DAO.UserDAO;
import com.hw.db.DAO.ForumDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;

import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import static org.mockito.Mockito.*;

// Full branch coverage for function UserList
class forumDAOTests {
    JdbcTemplate mockJdbc;
    ForumDAO forum;
    UserDAO.UserMapper USER_MAPPER;

    @BeforeEach
    void init() {
        mockJdbc = mock(JdbcTemplate.class);
        forum = new ForumDAO(mockJdbc);
        USER_MAPPER = new UserDAO.UserMapper();
    }

    @Test
    @DisplayName("Users list test #1")
    void ThreadListTest1() {
        ForumDAO.UserList("slug", null, null, false);
        verify(mockJdbc).query(
            Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"),
            Mockito.any(Object[].class),
            Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("Users list test #2")
    void ThreadListTest2() {
        ForumDAO.UserList("slug", null, "15.03.2023", false);
        verify(mockJdbc).query(
            Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"),
            Mockito.any(Object[].class),
            Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("Users list test #3")
    void ThreadListTest3() {
        ForumDAO.UserList("slug", null, "15.03.2023", true);
        verify(mockJdbc).query(
            Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;"),
            Mockito.any(Object[].class),
            Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("Users list test #4")
    void ThreadListTest4() {
        ForumDAO.UserList("slug", null, null, true);
        verify(mockJdbc).query(
            Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc;"),
            Mockito.any(Object[].class),
            Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("Users list test #5")
    void ThreadListTest5() {
        ForumDAO.UserList("slug", 1, null, false);
        verify(mockJdbc).query(
            Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname LIMIT ?;"),
            Mockito.any(Object[].class),
            Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("Users list test #6")
    void ThreadListTest6() {
        ForumDAO.UserList("slug", 1, null, true);
        verify(mockJdbc).query(
            Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc LIMIT ?;"),
            Mockito.any(Object[].class),
            Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("Users list test #7")
    void ThreadListTest7() {
        ForumDAO.UserList("slug", 1, "15.03.2023", true);
        verify(mockJdbc).query(
            Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
            Mockito.any(Object[].class),
            Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("Users list test #8")
    void ThreadListTest8() {
        ForumDAO.UserList("slug", 1, "15.03.2023", false);
        verify(mockJdbc).query(
            Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;"),
            Mockito.any(Object[].class),
            Mockito.any(UserDAO.UserMapper.class));
    }
}
