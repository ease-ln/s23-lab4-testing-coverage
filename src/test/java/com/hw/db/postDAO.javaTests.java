import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;

import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import static org.mockito.Mockito.*;
import java.sql.Timestamp;

// Full Basis path coverage for function setPost
class postDAOTests {
    JdbcTemplate mockJdbc;
    PostDAO mockPost;
    int postId = 10;
    Post post;

    @BeforeEach
    void init() {
        mockJdbc = mock(JdbcTemplate.class);
        mockPost = new PostDAO(mockJdbc);
        post = new Post("author", new Timestamp(10), "forum", "message", 0, 0, false);
        Mockito.when(mockJdbc.queryForObject(
            Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), 
            Mockito.any(PostDAO.PostMapper.class),
            Mockito.eq(postId))).thenReturn(post);
    }

    @Test
    @DisplayName("Post test #1")
    void SetPostTest1() {
        PostDAO.setPost(postId, post);
        Mockito.verify(mockJdbc).queryForObject(
            Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
            Mockito.any(PostDAO.PostMapper.class),
            Mockito.eq(postId));
    }


    @Test
    @DisplayName("Post test #2")
    void SetPostTest2() {
        Post newPost = new Post("author2", new Timestamp(13), "forum2", "message2", 0, 0, false);
        PostDAO.setPost(postId, newPost);
        Mockito.verify(mockJdbc).update(
            Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
            Mockito.eq("author2"),
            Mockito.eq("message2"),
            Mockito.eq(new Timestamp(13)),
            Mockito.eq(postId));
    }

    @Test
    @DisplayName("Post test #3")
    void SetPostTest3() {
        Post newPost = new Post("author2", new Timestamp(10), "forum2", "message2", 0, 0, false);
        PostDAO.setPost(postId, newPost);
        Mockito.verify(mockJdbc).update(
            Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"),
            Mockito.eq("author2"),
            Mockito.eq("message2"),
            Mockito.eq(postId));
    }

    @Test
    @DisplayName("Post test #4")
    void SetPostTest4() {
        Post newPost = new Post("author2", new Timestamp(13), "forum2", "message", 0, 0, false);
        PostDAO.setPost(postId, newPost);
        Mockito.verify(mockJdbc).update(
            Mockito.eq("UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
            Mockito.eq("author2"),
            Mockito.eq(new Timestamp(13)),
            Mockito.eq(postId));
    }

    @Test
    @DisplayName("Post test #5")
    void SetPostTest5() {
        Post newPost = new Post("author2", new Timestamp(10), "forum2", "message", 0, 0, false);
        PostDAO.setPost(postId, newPost);
        Mockito.verify(mockJdbc).update(
            Mockito.eq("UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"),
            Mockito.eq("author2"),
            Mockito.eq(postId));
    }

    @Test
    @DisplayName("Post test #6")
    void SetPostTest6() {
        Post newPost = new Post("author", new Timestamp(13), "forum2", "message3", 0, 0, false);
        PostDAO.setPost(postId, newPost);
        Mockito.verify(mockJdbc).update(
            Mockito.eq("UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
            Mockito.eq("message3"),
            Mockito.eq(new Timestamp(13)),
            Mockito.eq(postId));
    }

    @Test
    @DisplayName("Post test #7")
    void SetPostTest7() {
        Post newPost = new Post("author", new Timestamp(10), "forum2", "message3", 0, 0, false);
        PostDAO.setPost(postId, newPost);
        Mockito.verify(mockJdbc).update(
            Mockito.eq("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
            Mockito.eq("message3"),
            Mockito.eq(postId));
    }

    @Test
    @DisplayName("Post test #8")
    void SetPostTest8() {
        Post newPost = new Post("author", new Timestamp(13), "forum2", "message", 0, 0, false);
        PostDAO.setPost(postId, newPost);
        Mockito.verify(mockJdbc).update(
            Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
            Mockito.eq(new Timestamp(13)),
            Mockito.eq(postId));
    }
}